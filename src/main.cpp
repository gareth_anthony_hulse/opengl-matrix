#include <GLFW/glfw3.h>
#include <iostream>
#include <unistd.h>
#include <cmath>

void
gluPerspective (GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar)
{
    GLdouble xmin, xmax, ymin, ymax;

    ymax = zNear * tan( fovy * M_PI / 360.0 );
    ymin = -ymax;
    xmin = ymin * aspect;
    xmax = ymax * aspect;

    glFrustum( xmin, xmax, ymin, ymax, zNear, zFar );
}

void
draw_circle (float red, float green, float blue)
{
    const float
      steps = 200,
      angle = M_PI * 2.f / steps;

    float
      x_pos = 0,
      y_pos = 0,
      radius = 1.0f,
      prev_x = x_pos,
      prev_y = y_pos - radius; 
    
    for (int i = 0; i <= steps; i++)
      {
	float
	  new_x = radius * sin (angle * i),
	  new_y = -radius * cos (angle * i);
	
	glBegin (GL_TRIANGLES);
	glColor3f (red, green, blue);
	glVertex3f (0.0f, 0.0f, 0.0f);
	glVertex3f (prev_x / 2, prev_y / 2, 0.0f);
	glVertex3f (new_x / 2, new_y / 2, 0.0f);
	glEnd ();
	  
	prev_x = new_x;
	prev_y = new_y;
      }
}

int
main ()
{  
  GLFWwindow *window;
  if (!glfwInit ())
    {
      std::cerr << "GLFW failed to initalise!" << std::endl;
      exit (EXIT_FAILURE);
    }

  GLFWmonitor* monitor = glfwGetPrimaryMonitor ();
  const GLFWvidmode* mode = glfwGetVideoMode (monitor);
  glfwWindowHint (GLFW_RED_BITS, mode->redBits);
  glfwWindowHint (GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint (GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint (GLFW_REFRESH_RATE, mode->refreshRate);

  window = glfwCreateWindow (mode->width, mode->height, PACKAGE, monitor, nullptr);
  if (!window)
    {
      glfwTerminate ();
      std::cerr << "Failed to create window!" << std::endl;
      exit (EXIT_FAILURE);
    }
  
  glfwMakeContextCurrent(window);

  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity ();
  glScalef (0.1, 0.1, 1);

  float
    angle = 0,
    angle_moon = 0;

  int
    width,
    height;

  /* Render loop. */
  while (!glfwWindowShouldClose (window))
    {
      glfwSetInputMode (window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
      /* Resize frame buffer and viewport. */
      //gluPerspective(60, (float)width/height, 0.01f, 1.0f);
      glfwGetFramebufferSize (window, &width, &height);
      glViewport (0, 0, width, height);
      
      ++angle;
  
      glClearColor (0, 0, 0, 0);
      glClear (GL_COLOR_BUFFER_BIT);

      /* Sun. */
      draw_circle (1, 1, 0);

      {
	/* Earth. */
	glPushMatrix ();
	glRotatef (angle, 0, 0, 1);
	glTranslatef (0, 5, 0);
	glScalef (0.6, 0.6, 1);
	draw_circle (0, 0.3, 1);

	{
	  /* Moon. */
	  glPushMatrix ();
	  glRotatef (angle_moon, 0, 0, 1);
	  glTranslatef (0, 3, 0);
	  glScalef (0.5, 0.5, 1);
	  draw_circle (0.5, 0.5, 0.5);
	  glPopMatrix ();
	  angle_moon += 2;
	}
	
	glPopMatrix ();
      }
      
      glfwSwapBuffers (window);
      glfwPollEvents ();
    }
}
